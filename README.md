Fork of https://github.com/WhalesState/KnightVSBishops / https://godotengine.org/asset-library/asset/880.

# Knights vs Bishops

You play as the knight and your goal is to keep the bishops count smaller than 10, if you captured a bishop you will play again, how long can you survive?

## License

Copyright (C) 2021  Abdurrahmaan Iqbal

Licensed under the MIT license
